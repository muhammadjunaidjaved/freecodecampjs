var num = 5; // in-line comment

/* 

this is a multi-line comment:

We can put in 
as much text as 
we want.

*/

/* Data types: 

undefined, null, boolean, string, symbol, number, and object

*/ 

var myName = "Junaid"

let ourName = "JSCourse"

const pi = 3.14

var a;

console.log(a);

var b = 7; 

a = 8; 

b = a;

console.log(a);

// Intializing Variables with Assignment Operators 

var a = 5; 
var b = 1; 
var c = " I am a "; 


a = a + 1; 
b = b + 5; 
c = c + "String!";


// Declarations 
var studlyCapVar;
var properCamelCase;
var titleCaseOver; 

// Assignments 
studlyCapVar = 10;
properCamelCase = " A String";
titleCaseOver = 9000;


//Adding numbers
var sum = 10 + 0; 
console.log( "sum of 10 + 0 is: " + sum);

// Subtraction 
var sub = 46 - 1;
console.log("Subtracting 1 from 46: " + sub)

//Multiplication 
var multiply = 8 * 2;
console.log("8 * 2 is: " + multiply);

//Division
var quotient = 66 / 33;
console.log("66 / 33 is: "+quotient);

//Incrementing 
var myVar = 87;

myVar = myVar + 1; // myVarr++ is handy way to increment

//Decrementing 
myVar = myVar - 1; // myVar-- is also valid

//Decimal 
var decimalNum = 6.7;


//Remainder
var remainder;
remainder = 9 / 3;

//Compound Assignment with Augmented Addition
var a = 3;
a += 12;

//Compound Assignment with Augmented Subtraction
a -= 12;

//Compound Assignment with Augmented Multiplication
a *= 12;

//Compound Assignment with Augmented Division
a /= 12;

//Declare String Variables
var firstName = "Moulana";
var lastName = "Rumi";

//Escaping Literal Quotes in Strings 
var myStr = "I am a \"double quoted\" string inside \"double quotes\". ";
console.log(myStr);

//Quoting strings with single quotes
var myStrSingleQuote = `I am a "double quoted" string inside a "single quotes". `;
console.log(myStrSingleQuote);

//Escaping Sequences in Strings
/***** 
 * 
\` single quote
\" double quote 
\\ backslash
\n newline 
\r carriage return 
\t tab
\b backspace 
\f form feed

*****/

var myStrExample = "FirstLine \n\t\\SecondLine\nThirdLine";
console.log(myStrExample);

//Concatenating Strings with Plus Operator
var outStr = "I come first. " + "I come second.";

var myStrA = "This is the start. " + "This is the end.";
console.log(myStrA);

//Concatenating Strings with Plus Equals Operator
var ourStr = "I come first. ";
ourStr += "I come second";

//Constructing Strings with Variables
var ourName1a = "freeCodeCamp";
var ourStr = "Hello, our name is "+ ourName + ", How are you?";

var myName1a = "Junaid";
var myStr = "My name is " + myName + "and I am good!";

//Appending Variables with Strings 
var anAdjective = "awesome!";
var ourStr = "freeCodeCamp is ";
ourStr +=  anAdjective;
console.log(ourStr);

//Find Length of a String
var firstNameLength = 0;
var firstName = "firstName";

firstNameLength = firstName.length;

//Bracket Notation to Find First Character in String 
firstNameLength = firstName.length[0];

//String Immutability
var myStr = "Jello World";
mrStr = "Hello World!";

//Bracket Notation to Find Nth Character in String
var lastName = "Lovaless";
var thirdLetterOfLastName = lastName[2];

//Bracket Notation to Find Last Character in String
var lastLetterOfFirstName = firstName[firstName.length - 1];
var lastLetterOfLastName  = lastName[lastName.length - 1];

//All about string
var thirdToLastLetterOfFirstName = firstName[firstName.length - 3];
var secondToLastLetterOfLastName = lastName[lastName.length - 2];

function wordBlanks(myNoun, myAdjective, myVerb, myAdverb) {
    var result = "";
    result += "The " + myAdjective + " " + myNoun + " " + myVerb + " to the store " + myAdverb + "!";
    return result;
}
console.log(wordBlanks("dog", "big", "ran", "quickly"));
console.log(wordBlanks("bike", "slow", "flew", "slowly"));

//Arrays
var ourArray = ["John", 23];
var myArray = ["Kiwi", 1];

//Nested Arrays
var ourArray = [["The world", 12], ["Everything & Anyting", 101010]];
var myArray = [["The idea", 127], ["Another idea", 1122]];

//Access Array Data with Indexes 
var ourArray = [50, 60, 70];
var ourData = ourArray[0]; //equals 50
ourArray[1] = 45; //Modify

var myArray = [[1,2,3], [4,5,6], [7,8,9], [[10, 11, 12], 13, 14]];
var myData = myArray[2][0];
console.log(myData); 

//push()
var ourArray = ["Stimpson", "J", "cat"];
ourArray.push(["happy", "joy"]);
console.log(ourArray);

var myArray = [["John", 23], ["cat", 2]];
myArray.push(["dog", 3]);

//pop()
var ourArray = [1,2,3];
var removedFromOurArray = ourArray.pop(); //removedFromOurArray is equal 3, and ourArray now equals [1,2]

var myArray = [["John", 23], ["cat", 3]];
var removedFromMyArray = myArray.pop();
console.log(myArray);

//shift()
var myArray = ["Stimpson", "J", "cat"];
var removedFromOurArray = myArray.shift(); //removedFromOurArray now equals "Stimpson" and ourArray now equals ["J", "cat"]

var myArray = [["John", 23], ["cat", 3]];
var removedFromMyArray = myArray.shift();
console.log(myArray);

//unshift()
var ourArray = ["Stimpson", "J", "cat"];
ourArray.shift();
ourArray.unshift("Happy");

var myArray = [["John", 23], ["cat", 3]];
myArray.shift();
myArray.unshift(["Paul", 35]);
console.log(myArray);

//shopping list
var myList = [["cereal", 3], ["milk", 2], ["bananas", 3], ["juice", 2], ["eggs", 12]];

//write re-usable code with functions()
function ourReusableFunction() {
    console.log("Hey World!");
}

ourReusableFunction();

function myReusableFunction() {
    console.log("Good Day!")
}

myReusableFunction();

//fucntion with values
function ourFunctionWithArgs(a, b) {
    console.log(a - b);
}

ourFunctionWithArgs(10, 5);

//another function
function functionWithArgs(a, b) {
    console.log(a + b);
}

functionWithArgs(10, 7);

//Global Scope and Functions
var myGlobal = 10;

function fun1() {
    oopsGlobal = 5;
}

function fun2() {
    var output = "";
    if (typeof myGlobal != "undefined") {
        output += "myGlobal: " + myGlobal;
    }
    if (typeof oopsGlobal != "undefined") {
        output += " oppsGlobal: " + oopsGlobal;
    }
    console.log(output);
}

fun1();
fun2();

// local scopes
function myLocalScope() {
    var myVarA = 5;
    console.log(myVarA);
}
myLocalScope();


//Global vs. Local Scope in Functions
var outerWear = "T-shirt";

function myOutfit() {
    var outerWear = "sweater";
    return outerWear;
}

console.log(myOutfit());
console.log(outerWear);

// Return a Value from a Function with Return
function minusSeven(num) {
    return num - 7;
}

console.log(minusSeven(10));

function timesFive(num) {
    return num * 5;
}

console.log(timesFive(5));

//Understanding Undefined Value Returned from a Function
var sum = 0;
function addThree() {
    sum = sum + 3;
}

function addFive() {
    sum += 5;
}

//Assignment with a Returned Value
var changed = 0;

function change(num) {
    return (num * 5) / 3;
}

changed = change(10);

console.log(changed);

var processed = 0; 

function processArg(num) {
    return (num + 3) / 5;
}

processed = processArg(18);
console.log(processed);

//Stand in Line
function nextInLine(arr, item) {
    arr.push(item);
    return arr.item; //return arr.shift(); will remove the 1st item in arr and add item at end
}

var testArr = [1, 2, 3, 4, 5];

console.log("Before: " + JSON.stringify(testArr));
console.log(nextInLine(testArr, 6));
console.log("After: " + JSON.stringify(testArr));

//Boolean values
function welcomeToBooleans() {
    return true;
}

//Use Conditional Logic with If Statments
function ourTrueOrFalse(isItTrue) {
    if(isItTrue) {
        return "Yes, it's true";
    }
    return "No, it's false";
}

console.log(ourTrueOrFalse(true));

//Comparison with the Equality Operator
function testEqual(val) {
    if(val === 10) {
        return "Equal";
    }

    return "Not Equal";
}

console.log(testEqual('10'));


//Comparison with the Strict Equality Operator
/* 
3 === 3   (True)
3 === '3' (Fasle)
*/

//Comparison with the Inequality Operator e.g. if(val != 102)

//Comparison with the Strict Inequality Operator e.g. if(val !== 102)

//Comparison with Greater Than or Equal To Operator
function testGreaterOrEqual(val) {
    if(val >= 20) {
        return "20 or over";
    }

if (val) {
    return "10 or Over"
}

return "Less than 10";
}

//Comparison with the Less Than Operator e.g. if (val < 25)

//Comparison with the Less Than or Equal to Operator e.g if (val <= 25) 

//Comparison with the Logical And Operator e.g. if (val <= 50 && val >= 25)

//Comparison with the Logical Or Operator e.g. if (val < 10 || val > 20) return "Outside"

//Else Statement e.g. if (val > 5) { return "Bigger than 5" } else { return "5 or Smaller"};

//Else If Statements 
function testElseIf(val) {
    if(val > 10) {
        return "Greater than 10";
    } else if (val > 5) {
        return "Smaller than 5";
    } else {
        return "Between 5 and 10";
    }
}

testElseIf(7);

//Logical Order in If Else Statements
function orderMyLogic(val) {
    if (val < 5) {
        return "Less than 5";
    } else if (val < 10) {
        return "Less than 10";
    } else {
        return "Greater than or equal to 10";
    }
}

console.log(orderMyLogic(3));

//Chaining If Else Statements 
function testSize(num) {
    if (num < 5) {
        return "Tiny";
    }
    else if (num < 10) {
        return "Small";
    }
    else if (num < 15) {
        return "Medium";
    }
    else if (num < 20) {
        return "Large";
    }
    else {
        return "Huge";
    }
}

console.log(testSize(17));

//Golf Code 
var names = ["Hole-in-one!","Eagle", "Birdie", "Par", "Bogey", "Double Bogey", "Go Home!"];
function golfScore(par, strokes) {
   if (strokes == 1) {
      return names[0];
   }  else if (strokes <= par - 2) {
      return names[1];
   }  else if (strokes == par - 1) {
      return names[2];
   }  else if (strokes == par) {
      return names[3];
   }  else if (strokes == par + 1) {
      return names[4];
   }  else if (strokes == par + 2) {
      return names[5];
   }  else if (strokes == par + 3) {
      return names[6];
   } 
}

console.log(golfScore(5, 2));

//Switch Statemnts
function caseInSwitch(val) {
    var answer = "";
    switch(val) {
        case 1: 
            answer = "alpha";
            break;
        case 2: 
            answer = "beta";
            break;
        case 3: 
            answer = "gamma";
            break;
        case 4: 
            answer = "delta";
            break;                        
    }

    return answer;
}

console.log(caseInSwitch(1));

//Default Options in Switch Statements
function switchOfStuff(val) {
    var answer = "";
    switch(val) {
        case "a": 
            answer = "apple";
            break;
        case "b": 
            answer = "bird";
            break;
        case "c": 
            answer = "cat";
            break;     
        default: 
            answer = "stuff";
            break;  
    }

    return answer;
}
console.log(switchOfStuff("a"));

//Multiple Identical Options in Switch Statements 
function sequentialSizes(val) {
    var answer = "";
    switch(val) {
        case 1:
        case 2:
        case 3:
            answer = "Low";
            break;
        case 4:
        case 5:
        case 6:
            answer = "Mid";
            break;
        case 7:
        case 8:
        case 9:
            answer = "High";
            break;                        

    }

    return answer;
 }

 console.log(sequentialSizes(7));

 //Replacing If Else Chains With Switch
 function chainToSwitch(val) {
    var answer = "";

    switch(val) {
        case "bob":
            answer = "marley";
            break;
        case 42:
            answer = "The Answer";
            break;
        case 1:
            answer = "There is no need to predend to be Number: 1";
            break; 
        case 99:
            answer = "The Answer is missed by this much!";
            break;                                   
        case 7:
            answer = "This is seven.";
            break;
    }
 }

 // return answer;

 //Returning Boolean Values from Functions
 function isLess(a,b) {
    return a < b;
 }

 console.log(isLess(10, 15));

 //Returning Early Pattern from Functions
 function abTest(a, b) {
    if (a < 0 || b < 0) {
        return undefined;
    }

        return Math.round(Math.pow(Math.sqrt(a) + Math.sqrt(b), 2));
 }

 console.log(abTest(2,2));

 //Counting Cards
 var count = 0;


 //Build JavaScript Objects
 var ourCat = {
    "name": "Camper",
    "legs": 4,
    "trails": 1,
    "friends":["some!"]
 };

 //Accessing Object Properties with Dot Notation
 var testObj = {
    "hat": "ballcap",
    "shirt": "jersey",
    "shoes": "cleats"
 };

 var hatValue = testObj.hat;
 var shirtValue = testObj.shirt;

 //Accessing Object Properties with Bracket Notation
 var testObj = {
    "an entree": "chickenburger",
    "my side": "veggies",
    "the drink": "water"
 };

 var entreeValue = testObj["an entree"];
 var drinkValue = testObj["the drink"];

 //Accessing Object Properties with Variables
 var testObj = {
    12: "someNum",
    16: "someNumB",
    19: "someNumC"
 };

 var playerNumber = 16;
 var player = testObj[playerNumber];

 //Updating Object Properties
 ourCat.name = "Camperonion";

 //Add New Properties to an Object
 ourCat.sound = "meow meow"; 

 //Setting cat's sound to only one "meow"
 ourCat['sound'] = "meow";

 //Delete Properties From an Object
 delete ourCat.sound;

 //Using Objects for Lookups
 function phoneticLookUp(val) {
    var result = "";


    var lookup = {
        "alpha": "adams",
        "bravo": "Boston",
        "charlie": "Chicago",
        "delta": "Denver",
        "echo": "Easy",
        "foxtrot": "Frank"
    };

   result = lookup[val];

   return result;
 }

 console.log(phoneticLookUp("delta"));

 //Testing Objects for Properties
 var myObj = {
    gift: "sony",
    pet: "cat",
    bed: "sleigh"
 };

 function checkObj(checkProp) {
    if (myObj.hasOwnProperty(checkProp)) {
        return myObj[checkProp];
    } else {
        return "Not Found!";
    }
    
 }

 console.log(checkObj("gift"));

 //Manipulating Comples Objects
 var goodSounds = [
 {
    "artist": "Mahir demo name",
    "title": "Peace",
    "release-year": 2010,
    "formats": [
    "CD",
    "8T",
    "LP"
    ],
    "gold": true
 },
{
     "artist": "Mahir demo 2",
     "title": "Peace Follow",
     "release-year": 2015,
     "formats": [
          "youtube video"
     ]
 }  

 ];

 //Accessing Nested Objects 
 var myStorage = {
    "car": {
        "inside": {
            "glove box": "maps",
            "passenger seat": "crumbs"
        },
        "outside": {
            "trunk": "jack"
        }
    }
 };

 var gloveBoxContents = myStorage.car.inside["glove box"];

 console.log(gloveBoxContents);

 //Accessing Nested Arrays
 var myPlants = [
    {
        type: "flowers",
        list: [
           "rose",
           "tulip",
           "dandelion" 
        ]
    },
    {
        type: "trees",
        list: [
           "fir",
           "pine",
           "birch" 
        ]
    }
 ];

 var secondTree = myPlants[1].list[1];
 console.log(secondTree);

 //Record Collection
 var collection = {
    "2541": {
        "album": "SomeAlbum",
        "artist": "Someone",
        "tracks": [
            "Let It Go",
            "You Give Charity"
        ]
    },
    "2460": {
        "album": "1997",
        "artist": "Queen",
        "tracks": [
            "1995",
            "Little Racoon"
        ]
    },
    "1275": {
        "artist": "Robby Dunne",
        "tracks": []
    },
    "5471": {
        "album": "Abbey Inc"
    }
 };

 //Keep a copy of the collection for tests
 var collectionCopy = (JSON.stringify(collection));

 //Only change code below this line
 function updateRecords(id, prop, value) {
    if (value === "") {
        delete collection[id][prop];
    } else if (prop === "tracks") {
        collection[id][prop] = collection[id][prop] || [];
        collection[id][prop].push(value);
    } else {
        collection[id][prop] = value;
    }

    return collection;
 }

//Alter values below to test your code
updateRecords(1275, "tracks", "test A");
updateRecords(1275, "tracks", "test B");
updateRecords(2460, "tracks", "test");

console.log(updateRecords(5471, "artist", "ABI"));

//Iterate with While Loop
var myArray = [];

var i = 0;
while (i < 5) {
    myArray.push(i);
    i++;
}



//Iterate with For Loops
var ourArray = [];

for (var i = 0; i < 5; i++) {
    ourArray.push(i);
}

console.log(ourArray);

//Iterate Odd Numbers with a For Loop
var ourArray = [];

for (var i = 1; i < 10; i += 2) {
    ourArray.push(i);
}

console.log(ourArray);

//Counting backwards with For Loop
var ourArray = [];

for (var i = 9; i > 0; i -= 2) {
    ourArray.push(i);
}

console.log(ourArray);

//Iterate Through an Array with a For Loop
var ourArr = [9, 10, 11, 12];
var ourTotal = 0;

for (var i = 0; i < ourArray.length-1; i++) {
    ourTotal += ourArr[i];
}

console.log(ourTotal);

//Nesting For Loops
function multiplyAll(arr) {
    var product = 1;

    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            product *= arr[i][j];
        } 
    }

    return product;
}

var product = multiplyAll([[1,2],[3,4],[5,6,7]]);
console.log(product);

//Iterate with Do...While Loop
var myArray = [];
var i = 10;

 do {
    myArray.push(i);
    i++;
} while (i < 5)

console.log(i, myArray);

//Profile lookup
var contacts = [
  {
        "firstName": "Akira",
        "lastName": "Laine",
        "number": "0543236543",
        "likes": ["Pizza", "Coding", "Browie Points"]
   },
   {
        "firstName": "Harry",
        "lastName": "Point",
        "number": "0994372684",
        "likes": ["Hog", "Sprinkler", "Hagy"]
   },
   {
        "firstName": "Sherlock",
        "lastName": "Home",
        "number": "0487345643",
        "likes": ["Intrigue Point", "Viola"]
   },
   {
    "firstName": "Chris",
    "lastName": "Stuart",
    "number": "unknown",
    "likes": ["Javascript", "Gaming", "Foxes"]
   }
];

function lookUpProfile(name,prop) {
    for (var i = 0; i < contacts.length; i++) {
        if(contacts[i].firstName === name) {
            return contacts[i][prop] || "No such property";
        }
    }
    return "No such contact";
}

var data = lookUpProfile("Chris", "likes",);

console.log(data);

//Generate Random Fractions
function randomFunctions() {

    return Math.random();
}

console.log(randomFunctions());

//Generate Random Whole Numbers
var randomNumberBetween0and19 = Math.floor(Math.random() * 20);

function randomWholeNum() {

    return Math.floor(Math.random() * 10);
}

console.log(randomWholeNum());

//Generate Random Whole Numbers within a Range
function ourRandomRange(ourMin, ourMax) {
    return Math.floor(Math.random() * (ourMax - ourMin + 1)) + ourMin;
}

ourRandomRange(1, 9);

function randomRange(myMin, myMax) {
    return Math.floor(Math.random() * (myMax - myMin + 1)) + myMin;
}

var myRandom = randomRange(5, 15);
console.log(myRandom);

//Use the parseInt Function
function strToInteger(str) {
    return parseInt(str);
}

strToInteger("56");

//Use the parseInt Function with a Radix
function convertToInteger(str) {
    return parseInt(str, 2);
}

convertToInteger("10011");

//Use the Conditional (Ternary) Operator

// condition ? statement-if-true : statement-if-false;

function checkEqual(a, b) {
    return a === b ? true : false;
}
checkEqual(1, 2);

//Use Multiple Conditional (Ternary) Operators
function checkSign(num) {
    return num > 0 ? "positive" : num < 0 ? "negative" : "zero"
}

console.log(checkSign(7));

//Difference Between the var and let Keywords
let catName = "Quincy";
let quote;

 catName = "Beau";

function catTalk() {
    "use strict";

    catName = "Oliver";
    quote = catName + " says Meow!";

} 
catTalk();

//Compare the Scopes of vars and let Keywords
function checkScope() {
    "use strict";
    let i = "fucntion scope";
    if(true) {
        let i = "block scope";
        console.log("Block scope i is: ", i);
    }
    console.log("Function scope i is: ", i);
    return i;
}

checkScope();

//Declare a Read-Only Variable with the const Keyword
function printManyTimes(str) {
    "use strict";

    const SENTENCE = str + " is amazing!"; // cannot re-assign sentence with const keyword

    for(let i = 0; i < str.length; i += 2) {
        console.log(SENTENCE);
    }
}

printManyTimes("The Food");

//Mutate an Array Declared with Const
const s = [5, 7, 2];
function editInPlace() {
    "use strict";

   // s = [2, 5, 7]; this will give you an error becuase of const, 
   // instead you should follow below approach
   s[0] = 2;
   s[1] = 5;
   s[2] = 7;
}
editInPlace();

console.log(s);

//Prevent object mutation
function freezeObj() {
    "use strict";
    const MATH_CONSTANTS = {
        PI: 3.14
    };
    Object.freeze(MATH_CONSTANTS);

    try {
        MATH_CONSTANTS.PI = 99;
    } catch (ex) {
        console.log(ex);
    }
    return MATH_CONSTANTS.PI;
}

const PI = freezeObj();
console.log(PI);

//Use Arrow Functions to Write Concise Anonymous Functions
const mango = () => new Date();

//Write Arrow Functions with Parameters

/* 

var myConcat = function(arr1, arr2) {
    return arr1.concat(arr2);
}; 

*/

// below we will re-write the above function with arrow function
const myConcat = (arr1, arr2) => arr1.concat(arr2);

console.log(myConcat([1, 2], [3, 4, 6]));

// Write High Order Arrow Functions
const realNumberArray = [4, 5.6, -9.8, 3.14, 42, 6, 8.34, -2];

const squareList = (arr) => {
    const squaredIntegers = arr.filter(num => Number.isInteger(num) && num > 0).map (x => x * x);
    return squaredIntegers;
};

const squaredIntegers = squareList(realNumberArray);
console.log(squaredIntegers);


// Write High Order Arrow Functions
const increment = (function() {
    return function increment(number, value = 1) {
        return number + value;
    };
 }) ();

console.log(increment(5, 2));
console.log(increment(5));

// Use the Rest Operator with Function Parameters
const sumAgain = (function() {
    return function sum(...args) { // ... is the rest operator
        return args.reduce((a,b) => a + b, 0);
    };
}) ();
console.log(sumAgain(1,2,3, 4));

// Use the Spread Operator to Evaluate Arrays In-Place
const arr1 = ['JAN', 'FEB', 'MAR', 'APR', 'MAY'];
let arr2;
(function() {
    arr2 = [...arr1];
    arr1[0] = 'tomoto'
}) ();
console.log(arr2);

// Use Destructuring Assignment to Assign Variables from Objects
var voxel = {x: 3.6, y: 7.4, z: 6.54};

const { x : d, y : e, z : f } = voxel; // d = 3.6, e = 7.4, f = 6.54;

const AVG_TEMPERATURES = {
    today: 77.5,
    tomorrow: 79
};

function getTempOfTmrw(avgTemperatures) {
    "use strict"
    // change code below this line
    const { today: tempOfTomorrow } = avgTemperatures;
    return tempOfTomorrow;
}

console.log(getTempOfTmrw(AVG_TEMPERATURES));

// Destructuring Assignment with Nested Objects
const LOCAL_FORCAST = {
    today: { min: 72, max: 83 },
    tomorrow: { min: 73.3, max: 84.6 }
}

function getMaxOfTmrw(forecast) {
    "use strict";

    const { tomorrow : { max : maxOfTomorrow }} = forecast;

    return maxOfTomorrow;
} 

console.log(getMaxOfTmrw(LOCAL_FORCAST));

// Use Desructuring Assignment to Assign Variables from Arrays
const [z, x, , y] = [1, 2, 3, 4, 5, 6];
console.log(z, x, y);

let g = 8, h = 6;
(() => {
    "use strict";
    [a, b] = [b, a];
});

console.log(g);
console.log(h);

// Use Destructuring Assignment with the Rest Operator
const source = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
function removeFirstTwo(list) {
    const [, , ...arr] = list;
    return arr;
}

const arr = removeFirstTwo(source);
console.log(arr);
console.log(source);

// Use Destructuring Assignment to Pass an Object as a Function's Parameters
const stats = {
    max: 56.78,
    standard_deviation: 4.34,
    median: 34.54,
    mode: 23.87,
    min: -0.75,
    average: 35.85
};

const half = (function() {
    return function half({ max, min }) {
        return (max + min) / 2.0;
    };
}) ();
console.log(stats);
console.log(half(stats));

// Create Strings Using Template Literals
const person = {
    name: "Oliver Hasbro",
    age: 56
};

// Template literal with multi-line and string interpolation
const greeting = `hi, my name is ${person.name}! 
I am ${person.age} years old.`;

console.log(greeting);

const result = {
    success: ["max-length", "no-amd", "prefer-arrow-functions"],
    failure: ["no-var", "var-on-top", "linebreak"],
    skipped: ["id-blacklist", "no-dup-keys"]
};

function makeList(arr) {
    const resultDisplayArray = [];
    for (let i = 0; i < arr.length; i++) {
        resultDisplayArray.push(`<li class = "text-warning">${arr[i]}</li>`)
    }
        return resultDisplayArray;
    
}

const resultDisplayArray = makeList(result.failure);

console.log(resultDisplayArray);

// Write Concise Object Literal Declarations Using Simple Fields 
const createPersonBio = (name, age, gender) => ({name, age, gender});

console.log(createPersonBio("Zaparo Haskell", 56, "male"));

// Write Concise Declarative Functions
const bicycle = {
    gear: 2, 
    setGear(newGear) {
        "use strict";
        this.gear = newGear;
    }
};

bicycle.setGear(3);
console.log(bicycle.gear);

// Use class Syntax to Define a Constructor Function
class SpaceShuttle { 
    constructor(targetPlanet) { 
    this.targetPlanet = targetPlanet;
   }
}

var zeus = new SpaceShuttle('Jupiter');

console.log(zeus.targetPlanet);

// Example 2
class PlanetEarth {
    constructor(destinationPoint) {
        this.destinationPoint = destinationPoint;
    }
}

var landingPoint = new PlanetEarth("Hunza Valley, Pakistan");
console.log(landingPoint.destinationPoint);

// Example 3
function makeClass() {
    class Vegetable {
        constructor(name) {
            this.name = name;
        }
    }

    return Vegetable;
}

const Vegetable = makeClass();
const carrot = new Vegetable('carrot');
console.log(carrot.name);

// Use getter and setters to Control Access to an object
class Book {
    constructor(author) {
        this_.author = author;
    }

    // getter 
    get writer() {
        return this._author;
    }

    // setter
    set writer(updateAuthor) {
        this._author = updatedAuthor;
    }
}

function makeClass() {
    class Thermostat {
        constructor(temp) {
            this._temp = 5/9 * (temp - 32);
        }
        get temperature() {
            return this._temp;
        }
        set temperature(updatedTemp) {
            this._temp = updatedTemp;
        }
    }
    return Thermostat;
}

const Thermostat = makeClass();
const thermos = new Thermostat(76);
let temp = thermos.temperature;
thermos.temperature = 26;
temp = thermos.temperature;

console.log(temp);

// Understand the Difference Between import and require
// import { capitalizeString } from "./string_function"
// const cap = capitalizeString("hi!");

// console.log(cap);


// Use export to Resuse a Code Block
const capitalizeString = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export { capitalizeString };

export const foo = "bar";
export const bar = "foo";

// Use * to Import Everything from a File
// import * as capitalizeStrings from "capitalize_strings" --- this is an example

// Create an Export Fallback with export default
// export default function subtract(x, y) {return x - y;}

// Import a Default Export 
// import subtract from "math_functions";
// subtract(7, 4);
